# Proyecto de Turnero Digital
La aplicación es un turnero digital el cual consiste en que un usuario llega al módulo, escoge la fila en la que quiere ser atendido, hace el registro y le es asignado un turno en la respectiva fila, por otro lado un asesor escoge la fila que está atendiendo y puede ir atendiendo los turnos según van llegando, finalmente existe una vista de 3 tablas que muestran los turnos en vivo para cada fila.
## Tecnologías usadas:
La aplicación fue elaborada con nuxt3 para el frontend y fastapi para el backend usando mongodb como base de datos.
Para levantar los contenedores se usan imágenes de node, Python y mongo
Tanto el front como el back tienen su Dockerfile para que pueda ser ejecutado el docker-compose.yaml y levantar todo el stack de la aplicación.

## Instalación

Para instalar el proyecto es necesario tener instalado Docker y Docker compose
Después se debe clonar el repositorio
Una vez dentro del repositorio se ejecutará el comando

```bash
  docker-compose up -d –build
```
Cuando las aplicaciones se levanten correctamente el frontend quedará funcionando en http://localhost:3000, la documentación de la API quedará disponible http://localhost:8000/docs , y mongo quedara servido en mongodb://localhost:27017 pero solo tendrá acceso la api mas no el usuario.
Para detener los contenedores se usa el comando
```bash
docker-compose down
```

## Autores
- [@St4ros](https://github.com/St4ros)
- [@dylantopg](https://www.github.com/dylantopg)
- [@angelabenrod](https://gitlab.com/angelabenrod)

